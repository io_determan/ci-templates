# GitLab CI

## Stage: test/package

### Maven

```yaml
stages:
- verify
- package

include:
- project: "io_determan/ci-templates"
  ref: "master"
  file: "/gitlab/.maven.yml"
```

| Variables             | Description                         | Default |
| --------------------- | ----------------------------------- | ------- |
| IO_PACKAGE_NAME       | Name of package `-DfinalName`       | `app`   |
| IO_JAVA_VERSION       | Java version to use `"8"` or `"11"` | `"11"`  |
| IO_PACKAGE_TYPE       | Java package type                   | `jar`   |
| MAVEN_OPTS            | Maven Opts                          | `-Dmaven.repo.local=.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true`   |
| MAVEN_CLI_OPTS        | Maven Opts                          | `--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=false -DdeployAtEnd=false` |
| MAVEN_CLI_OPTS_EXTRAS | Maven CLI opts                      |         |


### Node
```yaml
stages:
# - verify
- package

include:
- project: "io_determan/ci-templates"
  ref: "master"
  file: "/gitlab/.node.yml"
```